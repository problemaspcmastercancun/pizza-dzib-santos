<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="pizza.css">
    <title>Pizzas</title>
</head>
<body>
        <div class="colores">
                <form>
                  <input id="rectangulo" type="button" value="Vegetariana" onclick="fondo('#33FFCE')">
                  <input id="rectangulo2" type="button" value="Pastor" onclick="fondo2('red')">
                  <input id="rectangulo3" type="button" value="Hawaiana" onclick="fondo3('orange')">
                  <input id="rectangulo4" type="button" value="Napolitana" onclick="fondo4('yellow')">
                </form>
              </div>
        <aside id="pizzatitulo">
                <h1 id="nombrep">Pizzas el papu.</h1>
                <h2 id="nombrep">"Disfrute su pizza"</h2>
               
            <ul id="circle">
                <li>
                    <input id="rev" class="text" type="button"  onclick="come('#33FFCE')">
                </li>
                <li>
                    <input id="rev2" class="text" type="button"  onclick="come2('green')">
                </li>
                <li>
                    <input id="rev3" class="text" type="button"  onclick="come3('green')">
                </li>
                <li>
                    <input id="rev4" class="text" type="button"  onclick="come4('green')">
                </li>
                <li>
                        <input id="rev5" class="text" type="button"  onclick="come5('green')">
                </li>
                <li>
                        <input id="rev6" class="text" type="button"  onclick="come6('green')">
                </li>
                <li>
                        <input id="rev7" class="text" type="button"  onclick="come7('green')">
                </li>
                <li>
                        <input id="rev8" class="text" type="button"  onclick="come8('green')">
                </li>          
              </ul>
                
        </aside>
</body>
<script type="text/javascript">
    function fondo(color){
      document.getElementById('circle').style="background: #33FFCE;";
      document.getElementById('nombrep').innerHTML="Pizza vegetariana";
    }
    function fondo2(color){
      document.getElementById('circle').style="background: red;";
      document.getElementById('nombrep').innerHTML="Pizza de pastor";
    }
    function fondo3(color){
      document.getElementById('circle').style="background: orange;";
      document.getElementById('nombrep').innerHTML="Pizza Hawaiana";
    }
    function fondo4(color){
      document.getElementById('circle').style="background: yellow;";
      document.getElementById('nombrep').innerHTML="Pizza de Napolitana";

    }
    function come(color){
      document.getElementById('rev').style="background: grey;";
    }
    function come2(color){
      document.getElementById('rev2').style="background: grey;";
    }
    function come3(color){
      document.getElementById('rev3').style="background: grey;";
    }
    function come4(color){
      document.getElementById('rev4').style="background: grey;";
    }
    function come5(color){
      document.getElementById('rev5').style="background: grey;";
    }
    function come6(color){
      document.getElementById('rev6').style="background: grey;";
    }
    function come7(color){
      document.getElementById('rev7').style="background: grey;";
    }function come8(color){
      document.getElementById('rev8').style="background: grey;";
    }
</script>

</html>